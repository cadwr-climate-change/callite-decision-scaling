# CalLite [D]ecision-[S]caling

This repository performs a decision scaling analysis of the central valley water system simulated with CalLite 3.0. 

## Instructions

## Structure
	|— runbook
		contains R-markdown files that are meant to 
		be run in sequence using RStudio project notebook. 
	|— src :
		|— callite
		
		

## Contact
[Wyatt.Arnold@water.ca.gov](mailtto:Wyatt.Arnold@water.ca.gov)

## Contributors
- Andrew Schwarz
- Patrick Ray
- Sungwook Wi
- Wyatt Arnold
- Jordi Vasquez
- Matt Correa
